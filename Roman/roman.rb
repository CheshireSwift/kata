# encoding: UTF-8

# Provides a method to express numbers as Roman numerals.
module Roman
  # Need a list of the individual numerals.
  @digits = {
    0 => 'N',
    1 => 'I',
    5 => 'V',
    10 => 'X',
    50 => 'L',
    100 => 'C',
    500 => 'D',
    1000 => 'M'
  }

  # The "subtractive rule" allows a shorthand for numbers that are near the
  # next digit.
  @sub_digits = {
    4 => 1,
    9 => 1,
    40 => 10,
    90 => 10,
    400 => 100,
    900 => 100
  }

  # The core function we expose. Returns the numeral representation of the
  # given number.
  #
  # n: Integer to convert.
  # returns a string containing the numeral representation of n.
  def self.numeral(n)
    case
    when !n.between?(0, 3999) then cannot_represent
    when @digits.key?(n)      then @digits[n]
    when @sub_digits.key?(n)  then sub_digit(n)
    else                           normal_numeral(n)
    end
  end

  # Sentinel value returned when a number cannot be represented in Roman
  # numerals.
  def self.cannot_represent
    :CannotRepresent
  end

  private

  def self.normal_numeral(n)
    # Work out the leading digit/sub-digit.
    last_smaller = [last_smaller(n, @sub_digits), last_smaller(n, @digits)].max

    # A "normal numeral" is the largest digit/sub-digit followed by the numeral
    # representation of whatever is left.
    numeral(last_smaller) + numeral(n - last_smaller)
  end

  def self.sub_digit(n)
    # A sub-digit, where permitted, is constructed by prefixing a digit with a
    # numeral indicating how much less than the digit it is.
    offset = @sub_digits[n]
    numeral(offset) + numeral(n + offset)
  end

  def self.last_smaller(n, numbers_map)
    # Find the keys in the map that are no larger than our number and return
    # the largest. Use -1 when not found (which is less than any number in our
    # domain).
    last = numbers_map.keys
                      .sort
                      .reject { |digit| digit > n }
                      .last

    last || -1
  end
end
