# encoding: UTF-8

require 'roman'

describe Roman, '#numerals' do
  def expect_numeral(number, numeral)
    calculated_numeral = Roman.numeral(number)
    expect(calculated_numeral).to eq(numeral)
  end

  it 'cannot represent negatives' do
    expect_numeral(-481, Roman.cannot_represent)
    expect_numeral(-1, Roman.cannot_represent)
  end

  it 'returns N for 0' do
    expect_numeral(0, 'N')
  end

  it 'returns I for 1' do
    expect_numeral(1, 'I')
  end

  it 'returns II for 2' do
    expect_numeral(2, 'II')
  end

  it 'returns IV for 4' do
    expect_numeral(4, 'IV')
  end

  it 'returns V for 5' do
    expect_numeral(5, 'V')
  end

  it 'returns VI for 6' do
    expect_numeral(6, 'VI')
  end

  it 'returns VII for 7' do
    expect_numeral(7, 'VII')
  end

  it 'returns VIII for 8' do
    expect_numeral(8, 'VIII')
  end

  it 'returns IX for 9' do
    expect_numeral(9, 'IX')
  end

  it 'returns X for 10' do
    expect_numeral(10, 'X')
  end

  it 'returns XII for 12' do
    expect_numeral(12, 'XII')
  end

  it 'returns XL for 40' do
    expect_numeral(40, 'XL')
  end

  it 'returns XLIV for 44' do
    expect_numeral(44, 'XLIV')
  end

  it 'returns MMMCMXCIX for 3999' do
    expect_numeral(3999, 'MMMCMXCIX')
  end

  it 'cannot represent numbers over 3999' do
    expect_numeral(4000, Roman.cannot_represent)
    expect_numeral(45_831, Roman.cannot_represent)
  end
end
