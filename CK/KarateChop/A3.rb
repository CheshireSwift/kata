require 'test/unit'

class TestChop < Test::Unit::TestCase

  def test_chop
    assert_equal(-1, chop(3, []))
    assert_equal(-1, chop(3, [1]))
    assert_equal(0,  chop(1, [1]))
    #
    assert_equal(0,  chop(1, [1, 3, 5]))
    assert_equal(1,  chop(3, [1, 3, 5]))
    assert_equal(2,  chop(5, [1, 3, 5]))
    assert_equal(-1, chop(0, [1, 3, 5]))
    assert_equal(-1, chop(2, [1, 3, 5]))
    assert_equal(-1, chop(4, [1, 3, 5]))
    assert_equal(-1, chop(6, [1, 3, 5]))
    #
    assert_equal(0,  chop(1, [1, 3, 5, 7]))
    assert_equal(1,  chop(3, [1, 3, 5, 7]))
    assert_equal(2,  chop(5, [1, 3, 5, 7]))
    assert_equal(3,  chop(7, [1, 3, 5, 7]))
    assert_equal(-1, chop(0, [1, 3, 5, 7]))
    assert_equal(-1, chop(2, [1, 3, 5, 7]))
    assert_equal(-1, chop(4, [1, 3, 5, 7]))
    assert_equal(-1, chop(6, [1, 3, 5, 7]))
    assert_equal(-1, chop(8, [1, 3, 5, 7]))
  end

end

class Chopper

  attr_reader :list

  def initialize(list)
    @list = list
  end

  def length
    @list.length
  end

  def midpoint
    length/2
  end

  def pivot
    @list[midpoint]
  end

  def lower
    @list[0..midpoint-1]
  end

  def upper
    @list[midpoint..length-1]
  end

  def find(elem)
    case
    when length < 1     then -1
    when elem == pivot  then midpoint
    when length == 1    then -1
    when elem < pivot   then sub_find(elem, lower, 0)
    when elem > pivot   then sub_find(elem, upper, midpoint)
    end
  end

  def sub_find(elem, slice, offset)
    f = Chopper.new(slice).find(elem)
    f > -1 ? f + offset : -1
  end

end

def chop(elem, arr)
  Chopper.new(arr).find(elem)
end

#puts chop(3, [1, 3, 5])
