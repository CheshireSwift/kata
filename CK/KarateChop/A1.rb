require 'test/unit'

class TestChop < Test::Unit::TestCase

  def test_chop
    assert_equal(-1, chop(3, []))
    assert_equal(-1, chop(3, [1]))
    assert_equal(0,  chop(1, [1]))
    #
    assert_equal(0,  chop(1, [1, 3, 5]))
    assert_equal(1,  chop(3, [1, 3, 5]))
    assert_equal(2,  chop(5, [1, 3, 5]))
    assert_equal(-1, chop(0, [1, 3, 5]))
    assert_equal(-1, chop(2, [1, 3, 5]))
    assert_equal(-1, chop(4, [1, 3, 5]))
    assert_equal(-1, chop(6, [1, 3, 5]))
    #
    assert_equal(0,  chop(1, [1, 3, 5, 7]))
    assert_equal(1,  chop(3, [1, 3, 5, 7]))
    assert_equal(2,  chop(5, [1, 3, 5, 7]))
    assert_equal(3,  chop(7, [1, 3, 5, 7]))
    assert_equal(-1, chop(0, [1, 3, 5, 7]))
    assert_equal(-1, chop(2, [1, 3, 5, 7]))
    assert_equal(-1, chop(4, [1, 3, 5, 7]))
    assert_equal(-1, chop(6, [1, 3, 5, 7]))
    assert_equal(-1, chop(8, [1, 3, 5, 7]))
  end

end


def chop(elem, arr)
  if arr.length == 1 && arr[0] != elem
    return -1
  end

  pivot_index = arr.length/2
  pivot = arr[pivot_index]
  case
  when pivot.nil?
    -1
  when pivot == elem
    #puts "Found elem: #{pivot}"
    pivot_index
  when elem < pivot
    #puts "pivot #{pivot} at #{pivot_index} - going lower for #{elem} with #{arr[0..pivot_index-1]}"
    res = chop(elem, arr[0..pivot_index-1])
    res != -1 ? res : -1
  when elem > pivot
    #puts "pivot #{pivot} at #{pivot_index} - going higher for #{elem} with #{arr[pivot_index..arr.length-1]}"
    res = chop(elem, arr[pivot_index..arr.length-1])
    res != -1 ? res + pivot_index : -1
  end
end

#puts chop(1, [1, 3, 5])
