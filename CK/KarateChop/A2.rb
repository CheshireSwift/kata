require 'test/unit'

class TestChop < Test::Unit::TestCase

  def test_chop
    assert_equal(-1, chop(3, []))
    assert_equal(-1, chop(3, [1]))
    assert_equal(0,  chop(1, [1]))
    #
    assert_equal(0,  chop(1, [1, 3, 5]))
    assert_equal(1,  chop(3, [1, 3, 5]))
    assert_equal(2,  chop(5, [1, 3, 5]))
    assert_equal(-1, chop(0, [1, 3, 5]))
    assert_equal(-1, chop(2, [1, 3, 5]))
    assert_equal(-1, chop(4, [1, 3, 5]))
    assert_equal(-1, chop(6, [1, 3, 5]))
    #
    assert_equal(0,  chop(1, [1, 3, 5, 7]))
    assert_equal(1,  chop(3, [1, 3, 5, 7]))
    assert_equal(2,  chop(5, [1, 3, 5, 7]))
    assert_equal(3,  chop(7, [1, 3, 5, 7]))
    assert_equal(-1, chop(0, [1, 3, 5, 7]))
    assert_equal(-1, chop(2, [1, 3, 5, 7]))
    assert_equal(-1, chop(4, [1, 3, 5, 7]))
    assert_equal(-1, chop(6, [1, 3, 5, 7]))
    assert_equal(-1, chop(8, [1, 3, 5, 7]))
  end

end


def chop(elem, arr)
  return -1 if arr.length == 0

  offset = 0
  while arr.length > 1 do
    #puts "arr is #{arr}"
    pivot_index = arr.length/2
    pivot = arr[pivot_index]
    #puts "pivot is #{pivot}"
    return pivot_index + offset if pivot == elem

    if elem < pivot
      arr = arr[0..pivot_index-1]
    else
      arr = arr[pivot_index..arr.length-1]
      offset = pivot_index
    end
  end

  arr[0] == elem ? 0 : -1

end

#puts chop(1, [1,3])
