require 'rspec'
require 'bounce_pit.rb'

describe BouncePit do
  it 'makes a pit' do
    expect(BouncePit.pit(100, 1).count '|').to eq(2)
  end

  it 'changes width' do
    expect(BouncePit.pit(100, 1)[0]).to eq('|')
    expect(BouncePit.pit(100, 1)[100 + 1]).to eq('|')
  end

  it 'changes height' do
    expect(BouncePit.pit(100, 73).lines.count).to eq(73)
  end
end
