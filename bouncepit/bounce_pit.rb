module BouncePit
  def self.pit(width, height)
    line = '|' + ' ' * width + '|'
    return (line + "\n") * (height - 1) + line
  end
end
